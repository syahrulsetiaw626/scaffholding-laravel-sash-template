<x-app-layout-component>
    <x-slot name="titlePage">Data Roles {{ $roles->name }}</x-slot>

    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title fw-bold">{{ str("list Permission in Role ". $roles->name)->title() }}</h3>
                </div>
                <div class="card-body">
                    <div class="w-100" id="fieldTable">
                        <div class="table-responsive">
                            <form action="" id="formRoles" method="post">
                                <table class="table dataTable">
                                    <thead class="table-primary">
                                        <tr>
                                            <th>No</th>
                                            <th>Permission</th>
                                            <th>
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name=""
                                                        id="select-all" value="option1">
                                                    <span class="custom-control-label"></span>
                                                </label>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($permissions as $i => $per)
                                        <tr>
                                            <td>{{ $i+1 }}</td>
                                            <td>{{ $per->name }}</td>
                                            <td>
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input checkPermission"
                                                        name="{{ $per->id }}" value="option1"
                                                        @foreach ($roles->permissions as $rolper)
                                                            @if ($rolper == $per->name)
                                                                checked
                                                            @endif
                                                        @endforeach
                                                        >
                                                    <span class="custom-control-label"></span>
                                                </label>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-primary">Submit Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-slot name="scriptVendor">
        <script src="../assets/plugins/select2/select2.full.min.js"></script>
        <script src="../assets/js/select2.js"></script>
        <script src="../assets/plugins/sweet-alert/sweetalert.min.js"></script>
    </x-slot>
    <x-slot name="script">
        <script>
            $(document).ready(function () {
                $('#select-all').click(function (event) {
                    if (this.checked) {
                        // Iterate each checkbox
                        $(':checkbox').each(function () {
                            this.checked = true;
                        });
                    } else {
                        $(':checkbox').each(function () {
                            this.checked = false;
                        });
                    }
                });

                let permissionsRegistered = [];
                var oTable = $(".dataTable").DataTable();
                $("#formRoles").submit(function(e){
                    e.preventDefault();
                    var rowcollection = oTable.$(".checkPermission:checked", {
                        "page": "all"
                    });
                    // console.log(rowcollection);
                    rowcollection.each(function (index, elem) {
                        var checkbox_value = $(elem).attr("name");
                        permissionsRegistered.push(parseInt(checkbox_value))
                    });
                    let data = {
                        'permissions' : JSON.stringify(permissionsRegistered),
                        'id' : "{{ $roles->id }}"
                    }

                    $.ajax({
                        type: "POST",
                        url: "{{ route('update.role.permission') }}",
                        data: data,
                        dataType: "json",
                        success: function (response) {
                            swal({
                                title: "Success!",
                                text: response.message,
                                type: "success"
                            });
                        },
                        error: function (response) {
                            swal({
                                title: "Failed!",
                                text: response.message,
                                type: "failed"
                            });
                        }
                    });
                })
            })

        </script>
    </x-slot>
</x-app-layout-component>
