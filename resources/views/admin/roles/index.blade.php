<x-app-layout-component>
    <x-slot name="titlePage">Data Roles</x-slot>

    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title fw-bold">{{ str("list role terdaftar")->title() }}</h3>
                </div>
                <div class="card-body">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal"
                        data-bs-target="#modalStore">
                        Create New Data
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="modalStore" data-bs-backdrop="static" data-bs-keyboard="false"
                        tabindex="-1" aria-labelledby="modalStoreLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalStoreLabel">Create New Role</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form action="" id="storeForm">
                                        <div class="row">
                                            <input class="form-control mb-4" name="name" placeholder="New Permission"
                                                type="text">
                                            <button type="submit" class="btn btn-primary w-100">Submit New Data</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100" id="fieldTable">
                        <div class="table-responsive">
                            <table class="table dataTable">
                                <thead class="table-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Guard Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($roles as $i => $role)
                                        <tr>
                                            <td>{{ $i+1 }}</td>
                                            <td>{{ $role->name }}</td>
                                            <td>{{ $role->guard_name }}</td>
                                            <td><a href="" class="btn btn-primary">Lihat Detail</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-slot name="script">
        <script>
            $(document).ready(function () {
                getTable()

                function getTable() {

                    $.ajax({
                        type: "POST",
                        url: "{{ url('api/ajax-data-table') }}",
                        cache: "false",
                        data: {
                            'headTable': "{{ $headTable }}",
                            'body': "{{ $body }}",
                            'link': "{{ $link }}",
                        },
                        datatype: "html",
                        beforeSend: function () {
                            //something before send
                        },
                        success: function (data) {
                            $('#fieldTable').html("");
                            $('#fieldTable').html(data);
                        }
                    });
                }

                $("#storeForm").submit(function (e) {
                    e.preventDefault();

                    let name = $(this).find('input').val();
                    let data = {
                        'name': name
                    }
                    $.ajax({
                        type: "POST",
                        url: "{{ url('api/roles') }}",
                        data: data,
                        dataType: "json",
                        success: function (response) {
                            swal({
                                title: "Success!",
                                text: response.message,
                                type: "success"
                            });
                            getTable()
                            $("#modalStore").modal('hide')
                        },
                        error: function (response) {
                            swal({
                                title: "Failed!",
                                text: response.message,
                                type: "failed"
                            });
                            $("#modalStore").modal('hide')
                        }
                    });

                    $(this)[0].reset();
                })
            })

        </script>
    </x-slot>
</x-app-layout-component>
