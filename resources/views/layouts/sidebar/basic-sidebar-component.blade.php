<li class="slide">
    <a class="side-menu__item has-link" data-bs-toggle="slide" href="{{ $link }}">
        {{ $icon }}
        <span class="side-menu__label">{{ str($text)->title() }}</span>
    </a>
</li>
