<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            ['name' => 'customer',],
            ['name' => 'superadmin'],
            ['name' => 'admin'],
            ['name' => 'purchasing'],
            ['name' => 'warehousing'],
        ])->each(function($data){
            Role::create($data);
        });
    }
}
