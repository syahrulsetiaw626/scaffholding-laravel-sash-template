<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AjaxTableController extends Controller
{
    public function ajaxDataTable(Request $request){
        // $head = str_replace('&quot;', '"', $request->get('headTable'));
        // $body = str_replace('&quot;', '"', $request->body);
        // $data['header'] = json_decode($head, true);
        // $data['body'] = json_decode($body, true);
        $data['link'] = $request->link;
        // return $data;
        return view('components.ajax-table-component', $data)->render();
    }
}
